# DB Dump CLI

Simple cli app to generate database dumps, useful in a backup process.

- Each table is dumped in a separated file
- Works only with MySQL databases for now (more to come if needed)

------

## Requirements

- `mysqldump`

## Installation

``` bash
composer global require ibrostudio/db-dump-cli
```

## Usage

**Dump**

``` bash
ibro-dcc dump <your-database-name> <your-database-username> <your-database-password> <your-database-host> <your-database-port>
```

`host` and `port` can be omitted (default values are 127.0.0.1 and 3306)

**Clean up**

To remove the dumps:

``` bash
ibro-dcc clean
```

## Credits

- [Laravel Zero](https://laravel-zero.com/)
- [Spatie db-dumper](https://github.com/spatie/db-dumper)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
