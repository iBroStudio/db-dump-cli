<?php

use App\MysqlDumper;
use Illuminate\Support\Facades\Storage;

it('runs clean command', function () {
    $this->expect(Storage::path(MysqlDumper::DUMP_DIRECTORY))->toBeReadableDirectory();

    $this->artisan('clean')
        ->assertExitCode(0);
});

it('removes dumps files and their directory', function () {
    $this->expect(
        is_dir(Storage::path(MysqlDumper::DUMP_DIRECTORY))
    )->toBeFalse();
});