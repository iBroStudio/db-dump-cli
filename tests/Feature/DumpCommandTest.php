<?php

use App\MysqlDumper;
use Illuminate\Support\Facades\Storage;

it('runs dump command', function () {
    Storage::deleteDirectory(MysqlDumper::DUMP_DIRECTORY);

    $this->expect(
        is_dir(Storage::path(MysqlDumper::DUMP_DIRECTORY))
    )->toBeFalse();

    $this->artisan('dump', [
        'database' => 'test',
        'username' => 'root',
        'password' => '',
    ])
        ->assertExitCode(0);
});

it('creates a directory for dumps', function () {
    $this->expect(Storage::path(MysqlDumper::DUMP_DIRECTORY))->toBeReadableDirectory();
});

it('dumps DB tables', function () {
    $dump = Storage::path(MysqlDumper::DUMP_DIRECTORY) . '/migrations.sql';

    $this->expect(file_exists($dump))->toBeTrue();
    $this->expect(file_get_contents($dump))->toContain('CREATE TABLE `migrations`');
});