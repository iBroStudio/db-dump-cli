<?php

namespace App;

interface DumperContract {

    public function configureDatabaseConnection(array $params);

    public function ensureDatabaseConnection();

    public function dumpTables();

}