<?php

namespace App\Commands;

use App\DumperManager;
use Exception;
use Illuminate\Support\Arr;
use LaravelZero\Framework\Commands\Command;

class DumpCommand extends Command
{
    CONST BACKUP_DIRECTORY = 'database-backup';

    protected $signature = 'dump {database} {username} {password} {host=127.0.0.1} {port=3306}';

    protected $description = 'Process a tables separated MYSQL dump';

    public function handle(DumperManager $dumper)
    {
        try {

            $this->task("Ensure dump directory", $dumper->ensureDumpDirectory());

            $this->task("Configure database connection",
             $dumper->configureDatabaseConnection(
                    Arr::except($this->arguments(), ['command'])
                )
            );

            $this->task("Ensure database connection",
             $dumper->ensureDatabaseConnection($this->argument('database'))
            );

            $this->task("Database dump", $dumper->dumpTables());

        } catch (Exception $e) {
            $this->error($e->getMessage());
        }
    }

}
