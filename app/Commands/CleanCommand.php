<?php

namespace App\Commands;

use App\DumperManager;
use LaravelZero\Framework\Commands\Command;

class CleanCommand extends Command
{
    protected $signature = 'clean';

    protected $description = 'Clean up dump files';

    public function handle(DumperManager $dumper)
    {
        $this->task("Deleting backup files", $dumper->clean());
    }
}
