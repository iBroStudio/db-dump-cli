<?php

namespace App;

use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

abstract class DumperCommon
{

    CONST DUMP_DIRECTORY = 'database-backup';

    protected $tables = [];

    public function ensureDumpDirectory()
    {
        $this->clean();

        if (! Storage::makeDirectory(self::DUMP_DIRECTORY)) {
            throw new Exception('Can\'t create backup directory');
        }

        chdir(Storage::path(self::DUMP_DIRECTORY));
    }

    public function clean()
    {
        Storage::deleteDirectory(self::DUMP_DIRECTORY);
    }

    protected function setConfiguration(string $configuration, array $params)
    {
        foreach ($params as $key => $value) {
            Config::set($configuration.$key, $value);
        }
    }
}