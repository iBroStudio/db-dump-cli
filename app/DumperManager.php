<?php

namespace App;

use App\MysqlDumper;
use Illuminate\Support\Manager;

class DumperManager extends Manager
{
    public function getDefaultDriver()
    {
        return 'mysql';
    }

    public function createMysqlDriver()
    {
        return new MysqlDumper;
    }
}
