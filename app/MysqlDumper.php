<?php

namespace App;

use App\DumperCommon;
use App\DumperContract;
use Illuminate\Support\Facades\DB;
use Spatie\DbDumper\Databases\MySql;

class MysqlDumper extends DumperCommon implements DumperContract
{
    public function configureDatabaseConnection(array $params)
    {
        $this->setConfiguration('database.connections.mysql.', $params);
    }

    public function ensureDatabaseConnection()
    {
        $tables = DB::select('SHOW TABLES');
        $this->tables = array_map('current',$tables);
    }

    public function dumpTables()
    {
        foreach ($this->tables as $table) {
            MySql::create()
                ->setHost(config('database.connections.mysql.host'))
                ->setPort(config('database.connections.mysql.port'))
                ->setDbName(config('database.connections.mysql.database'))
                ->setUserName(config('database.connections.mysql.username'))
                ->setPassword(config('database.connections.mysql.password'))
                ->includeTables($table)
                ->dumpToFile($table . '.sql');
        }
    }
}